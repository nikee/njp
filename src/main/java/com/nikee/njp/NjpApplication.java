package com.nikee.njp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NjpApplication {

	public static void main(String[] args) {
		SpringApplication.run(NjpApplication.class, args);
	}

}
